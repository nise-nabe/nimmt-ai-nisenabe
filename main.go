package main

import (
	"./fio"
	"./fio/command"
	"./fio/util"
	"./model"
	"./operation"
	"log"
	"os"
	"strings"
)

func main() {
	// cardset := &CardSet{isUsed: make([]bool, 105)}

	log.SetOutput(os.Stderr)
	s := fio.NewInOut(os.Stdin, os.Stdout)
	command.Players(s.NextLine())
	s.Println(util.EmptyResponse())
	_, cards := command.Cards(s.NextLine())
	s.Println(util.EmptyResponse())

	deck := model.NewDeck(104)

	hand := deck.NewHand(cards)

	for {
		str := s.NextLine()
		switch strings.Split(str, " ")[0] {
		case "rows":
			rows := command.Rows(str)
			cardsets := make([]model.CardSet, len(rows))
			for i, r := range rows {
				deck.Use(r...)
				cardsets[i] = r
			}
			solver := operation.NewSolver()
			result := solver(deck, hand, cardsets)
			s.Println(result)
		case "places":
			places := command.Places(str)
			deck.Use(places...)
			s.Println(util.EmptyResponse())
		case "info":
			s.Println(util.EmptyResponse())
		case "select":
			cards := command.Select(str)
			mini := operation.Select(cards)

			s.Println(mini)
		case "quit":
			s.Println(util.EmptyResponse())
			os.Exit(0)
		default:
			log.Fatalln("Unknown Command was received")
			os.Exit(0)
		}
		s.Flush()
	}
}
