package model

import (
	"testing"
	"sort"
)

func TestCreateHand(t *testing.T) {
	deck := NewDeck(40)
	hands := deck.CreateHands(4, 10)

	a := make([]int, 0)
	for _, h := range hands {
		a = append(a, h.Cards...)
	}
	sort.Ints(a)

	for i, v := range a {
		if i + 1 != v {
			t.Fatalf("invalid index: %d, value: %d\n", i, v)
		}

	}

}

func TestCreateHand2(t *testing.T) {
	deck := NewDeck(40)
	hands := deck.CreateHands(4, 5)

	a := make([]int, 0)
	for _, h := range hands {
		a = append(a, h.Cards...)
	}
	sort.Ints(a)

	b := make([]bool, 40 + 1)

	for _, v := range a {
		if b[v] {
			t.Fatalf("invalid value: %d\n", v)
		}
		b[v] = true
	}

}
