package model

import (
	"sort"
	"math/rand"
	"time"
)

type Hand struct {
	Cards CardSet
}

func (h *Hand) Get(idx int) int {
	result := h.Cards[idx]

	h.Cards = append(h.Cards[0:idx], h.Cards[idx+1:]...)

	return result
}

type CardSet []int

func (c CardSet) First() (int, int) {
	return 0, c[0]
}

func (c CardSet) Last() (int, int) {
	i := len(c) - 1
	return i, c[i]
}

type Deck struct {
	IsUsed []bool
}

func NewDeck(size int) *Deck {
	return &Deck{IsUsed: make([]bool, size+1)}
}

func (d *Deck) NewHand(nums []int) *Hand {
	var a CardSet
	for _, v := range nums {
		a = append(a, v)
	}
	hand := &Hand{Cards: a}
	sort.Ints(hand.Cards)

	d.Use(nums...)

	return hand
}

func (d *Deck) CreateHands(n, length int) []Hand {
	rand.Seed(time.Now().UnixNano())

	a := make([]int, 0)
	for i, b := range d.IsUsed {
		if i > 0 && !b {
			a = append(a, i)
		}
	}

	result := make([]Hand, 0)
	r := rand.Perm(len(a))
	for t := 0; t < n; t++ {
		var c CardSet
		for i := t * length; i < t * length + length; i++ {
			c = append(c, a[r[i]])
		}
		result = append(result, Hand{c})
	}

	return result
}

func (d *Deck) Use(nums... int) {
	for _, v := range nums {
		d.IsUsed[v] = true
	}
}

func Cost(n int) int {
	switch {
	case n == 55:
		return 7
	case n%11 == 0:
		return 5
	case n%10 == 0:
		return 3
	case n%5 == 0:
		return 2
	default:
		return 1
	}
	return 0 // for go 1.0
}
