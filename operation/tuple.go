package operation

type IndexTuple struct {
	Index int
	Value int
}

type IndexTuples []IndexTuple

func (t IndexTuples) Len() int {
	return len(t)
}

func (t IndexTuples) Less(i, j int) bool {
	return t[i].Value < t[j].Value
}

func (t IndexTuples) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}
