package operation

import (
	"testing"
)

func TestUpperBound(t *testing.T) {

	type TestData struct {
		target []int
		value  int

		expected_i int
		expected_v int
	}

	data := []TestData{
		TestData{
			target:     []int{1, 2, 3, 9, 10},
			value:      6,
			expected_i: 2,
			expected_v: 3,
		},
		TestData{
			target:     []int{1, 2, 3, 6, 9, 10},
			value:      6,
			expected_i: 3,
			expected_v: 6,
		},
	}

	for _, d := range data {
		i, v := UpperBound(d.target, d.value)

		if d.expected_i != i || d.expected_v != v {
			t.Errorf("Invalid. index: %d, value: %d", i, v)
		}
	}
}
