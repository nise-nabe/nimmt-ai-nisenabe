package operation

import (
	"testing"
	"sort"
)

func TestIndexTupleSort(t *testing.T) {
	var a IndexTuples
	for i := 0; i < 5; i++ {
		a = append(a, IndexTuple{Index:i, Value: 4 - i})
	}
	sort.Sort(a)

	for i, v := range a {
		if i != v.Value && i != 4 - v.Index {
			t.Fatalf("invalid")
		}
	}
}
