package operation

func UpperBound(a []int, val int) (int, int) {
	for i := len(a) - 1; i >= 0; i-- {
		if a[i] <= val {
			return i, a[i]
		}
	}

	return -1, -1
}
