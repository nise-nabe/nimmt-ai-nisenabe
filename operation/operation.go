package operation

import (
	. "../model"
	"math/rand"
	"sort"
	"strconv"
)

func Select(cards [][]string) int {
	mini, min := 99999, 99999
	for index, x := range cards {
		count := 0
		for _, y := range x {
			yy, _ := strconv.Atoi(y)
			count += Cost(yy)
		}
		if min > count {
			min = count
			mini = index
		}
	}

	return mini
}

type solver func(d *Deck, h *Hand, rows []CardSet) int

func NewSolver() solver {
	return solve
}

func randomSolve(d *Deck, h *Hand, rows []CardSet) int {
	return h.Get(rand.Intn(len(h.Cards)))
}

func maxSolve(d *Deck, h *Hand, rows []CardSet) int {
	return h.Get(len(h.Cards) - 1)
}

func defSolve(d *Deck, h *Hand, rows []CardSet) int {
	resulti, resultmin := -1, 9999
	score := make([]int, len(h.Cards))
	for idx, res := range h.Cards {
		min := 9999
		for _, x := range rows {
			_, y := x.Last()
			if y < res && min > res-y && len(x) < 5 {
				min = res - y
			}
		}

		if resultmin > min {
			resulti, resultmin = idx, min
		}
	}
	if resulti >= 0 {
		score[resulti] = 10
	} else {
		resulti, _ := h.Cards.Last()
		score[resulti] = 10
	}

	mi, _ := maxA(score)
	return h.Get(mi)
}

func solve(d *Deck, h *Hand, rows []CardSet) int {
	resulti, resultmin := -1, 9999
	score := make([]int, len(h.Cards))
	for idx, res := range h.Cards {
		min := 9999
		for _, x := range rows {
			_, y := x.Last()
			if y < res && min > res-y && len(x) < 5 {
				min = res - y
			}
		}

		if resultmin > min {
			resulti, resultmin = idx, min
		}
	}
	if resulti >= 0 {
		score[resulti] = 10
	} else if len(h.Cards) < 6 {
		score[simSolveN(d, h, rows)] = 6
	}

	lastCards := getLasts(rows)
	sort.Ints(lastCards)

	max := 0
	for i, v := range lastCards {
		upperi, upper := UpperBound(h.Cards, v)
		prevCard := 0
		if i > 0 {
			prevCard = lastCards[i-1]
		}

		if max < upper-prevCard {
			resulti, max = upperi, upper-prevCard
		}
	}

	if resulti >= 0 {
		score[resulti] = 6
	}

	mi, _ := maxA(score)
	return h.Get(mi)
}

func getLasts(cs []CardSet) []int {
	result := make([]int, len(cs))
	for i, v := range cs {
		_, result[i] = v.Last()
	}

	return result
}

func getLastsWithIndex(cs []CardSet) [][]int {
	result := make([][]int, len(cs))
	for i, v := range cs {
		ii, vv := v.Last()
		result[i] = []int{ii, vv}
	}

	return result
}

func maxA(a []int) (ri int, rv int) {
	for i, v := range a {
		if v >= rv {
			ri, rv = i, v
		}
	}

	return ri, rv
}

func minA(a []int) (ri int, rv int) {
	rv = 99999
	for i, v := range a {
		if v <= rv {
			ri, rv = i, v
		}
	}

	return ri, rv
}

func multiSolve(d *Deck, h *Hand, rows []CardSet) int {
	if len(h.Cards) > 3 {
		return simSolve(d, h, rows)
	}
	return maxSolve(d, h, rows)
}

// -----

type Player struct {
	d int
	h CardSet
}

type Players []Player

type Rows []CardSet


func simSolveN(d *Deck, h *Hand, rows []CardSet) int {
	score := make([]int, len(h.Cards))

	for t := 0; t < 5; t++ {
		hands := d.CreateHands(4, len(h.Cards))

		var P Players
		for i := 0; i < 4; i++ {
			P = append(P, Player{d:0, h:hands[i].Cards})
		}
		my := Player{d:0, h:h.Cards}

		scores := play(my, P, rows)
		for index, damage := range scores {
			score[index] += damage
		}
	}

	mi, _ := minA(score)
	return mi
}

func simSolve(d *Deck, h *Hand, rows []CardSet) int {
	return h.Get(simSolveN(d, h, rows))
}

func play(my Player, P Players, R Rows) []int {
	score := make([]int, len(my.h))

	for vi, v := range my.h {
		for _, v1 := range P[0].h {
			for _, v2 := range P[1].h {
				for _, v3 := range P[2].h {
					for _, v4 := range P[3].h {
						a := []int{v, v1, v2, v3, v4}
						rrr := sel(a, R)
						score[vi] += rrr[0]
					}
				}
			}
		}
	}

	return score
}

func sel(a []int, R Rows) [5]int {
	b := IndexTuples{}
	for i, v := range a {
		b = append(b, IndexTuple{Index:i, Value: v})
	}
	sort.Sort(b)

	score := make([]int, 5)

	for _, v := range b {
		var lasts IndexTuples
		for i, r := range R {
			_, last := r.Last()
			lasts = append(lasts, IndexTuple{ Index: i, Value: last })
		}
		sort.Sort(lasts)

		idx := -1
		for ii, l := range lasts {
			if v.Value < l.Value {
				break
			}
			idx = ii
		}

		cost := 0
		if idx >= 0 {
			if len(R[idx]) > 4{
				cost = countCost(R[idx])
				R[idx] = CardSet{}
			}
			R[idx] = append(R[idx], v.Value)
		} else {
			mini, min := 99999, 99999
			for index, r := range R {
				count := countCost(r)
				if min > count {
					min, mini = count, index
				}
			}

			R[mini] = CardSet{v.Value}
			cost = min
		}

		score[v.Index] += cost
	}

	var ranks IndexTuples
	for i, v := range score {
		ranks = append(ranks, IndexTuple{Index:i, Value:v})
	}

	sort.Sort(ranks)

	var result [5]int
	for i, v := range ranks {
		result[v.Index] = i
	}

	return result
}

func countCost(s CardSet) int {
	result := 0
	for _, c := range s {
	  result += Cost(c)
	}

	return result
}

