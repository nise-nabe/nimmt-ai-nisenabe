package main

import (
	"strconv"
	"log"
	"os"
	"strings"
	"io"
	"fmt"
	"bufio"
	"sort"
	"time"
	"math/rand"
)

func main() {
	// cardset := &CardSet{isUsed: make([]bool, 105)}

	log.SetOutput(os.Stderr)
	s := NewInOut(os.Stdin, os.Stdout)
	CPlayers(s.NextLine())
	s.Println(EmptyResponse())
	_, cards := Cards(s.NextLine())
	s.Println(EmptyResponse())

	deck := NewDeck(104)

	hand := deck.NewHand(cards)

	for {
		str := s.NextLine()
		switch strings.Split(str, " ")[0] {
		case "rows":
			rows := CRows(str)
			cardsets := make([]CardSet, len(rows))
			for i, r := range rows {
				deck.Use(r...)
				cardsets[i] = r
			}
			solver := NewSolver()
			result := solver(deck, hand, cardsets)
			s.Println(result)
		case "places":
			places := Places(str)
			deck.Use(places...)
			s.Println(EmptyResponse())
		case "info":
			s.Println(EmptyResponse())
		case "select":
			cards := CSelect(str)
			mini := Select(cards)

			s.Println(mini)
		case "quit":
			s.Println(EmptyResponse())
			os.Exit(0)
		default:
			log.Fatalln("Unknown Command was received")
			os.Exit(0)
		}
		s.Flush()
	}
}

// algo.go
func UpperBound(a []int, val int) (int, int) {
	for i := len(a) - 1; i >= 0; i-- {
		if a[i] <= val {
			return i, a[i]
		}
	}

	return -1, -1
}
// end algo.go

// io.go
// InOut
type InOut struct {
	*bufio.Reader
	*bufio.Writer
}

func NewInOut(r io.Reader, w io.Writer) *InOut {
	return &InOut{bufio.NewReader(r), bufio.NewWriter(w)}
}

// Get Next Line String
func (s *InOut) NextLine() (r string) {
	b, _ := s.ReadByte()
	for b == '\n' {
		b, _ = s.ReadByte()
	}
	buf := make([]byte, 0)
	for ; b != '\n'; b, _ = s.ReadByte() {
		buf = append(buf, b)
	}
	return string(buf)
}

// Print immediately with new line
func (s *InOut) Println(o interface{}) {
	fmt.Println(o)
}

// end io.go

// model.go
type Hand struct {
	Cards CardSet
}

func (h *Hand) Get(idx int) int {
	result := h.Cards[idx]

	h.Cards = append(h.Cards[0:idx], h.Cards[idx+1:]...)

	return result
}

type CardSet []int

func (c CardSet) First() (int, int) {
	return 0, c[0]
}

func (c CardSet) Last() (int, int) {
	i := len(c) - 1
	return i, c[i]
}

type Deck struct {
	IsUsed []bool
}

func NewDeck(size int) *Deck {
	return &Deck{IsUsed: make([]bool, size+1)}
}

func (d *Deck) NewHand(nums []int) *Hand {
	var a CardSet
	for _, v := range nums {
		a = append(a, v)
	}
	hand := &Hand{Cards: a}
	sort.Ints(hand.Cards)

	d.Use(nums...)

	return hand
}

func (d *Deck) CreateHands(n, length int) []Hand {
	rand.Seed(time.Now().UnixNano())

	a := make([]int, 0)
	for i, b := range d.IsUsed {
		if i > 0 && !b {
			a = append(a, i)
		}
	}

	result := make([]Hand, 0)
	r := rand.Perm(len(a))
	for t := 0; t < n; t++ {
		var c CardSet
		for i := t * length; i < t * length + length; i++ {
			c = append(c, a[r[i]])
		}
		result = append(result, Hand{c})
	}

	return result
}

func (d *Deck) Use(nums... int) {
	for _, v := range nums {
		d.IsUsed[v] = true
	}
}

func Cost(n int) int {
	switch {
	case n == 55:
		return 7
	case n%11 == 0:
		return 5
	case n%10 == 0:
		return 3
	case n%5 == 0:
		return 2
	default:
		return 1
	}
	return 0 // for go 1.0
}
// end model.go

// util.go
func SplitCards(str string) []string {
	return strings.Split(str, ",")
}

func EmptyResponse() string {
	return ""
}
// end util.go

// tuple.go
type IndexTuple struct {
	Index int
	Value int
}

type IndexTuples []IndexTuple

func (t IndexTuples) Len() int {
	return len(t)
}

func (t IndexTuples) Less(i, j int) bool {
	return t[i].Value < t[j].Value
}

func (t IndexTuples) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}
// end tuple.go

// command.go
func CPlayers(str string) []string {
	strs := strings.Split(str, " ")

	if strs[0] != "players" {
		panic("Invalid Players Command " + strs[0])
		os.Exit(1)
	}

	ns, players := strs[1], strs[2:]
	if n, err := strconv.Atoi(ns); err != nil || n != len(players) {
		panic("Invalid Input \"Players\"")
		os.Exit(1)
	}

	return players
}

func Cards(str string) (cardNum int, cards []int) {
	strs := strings.Split(str, " ")

	if strs[0] != "cards" {
		panic("Invalid Cards Command " + strs[0])
		os.Exit(1)
	}

	cardNum, err := strconv.Atoi(strs[1])

	if err != nil {
		panic(err)
		os.Exit(1)
	}

	for _, v := range SplitCards(strs[2]) {
		n, _ := strconv.Atoi(v)
		cards = append(cards, n)
	}

	return
}

func CRows(str string) [][]int {
	strs := strings.Split(str, " ")
	if strs[0] != "rows" {
		panic("Invalid Cards Command " + strs[0])
		os.Exit(1)
	}

	rows := make([][]int, 0)
	for _, x := range strs[1:] {
		row := make([]int, 0)
		for _, v := range SplitCards(x) {
			x, _ := strconv.Atoi(v)
			row = append(row, x)
		}

		rows = append(rows, row)
	}

	return rows
}

func CSelect(str string) [][]string {
	strs := strings.Split(str, " ")

	if strs[0] != "select" {
		panic("Invalid Cards Command " + strs[0])
		os.Exit(1)
	}

	result := make([][]string, len(strs[1:]))
	for index, x := range strs[1:] {
		result[index] = SplitCards(x)
	}

	return result
}

func Places(str string) []int {
	strs := strings.Split(str, " ")

	if strs[0] != "places" {
		panic("Invalid Cards Command " + strs[0])
		os.Exit(1)
	}

	result := make([]int, len(strs[1:]))
	for index, x := range strs[1:] {
		result[index], _ = strconv.Atoi(x)
	}

	return result
}
// end command.go

// operation.go
func Select(cards [][]string) int {
	mini, min := 99999, 99999
	for index, x := range cards {
		count := 0
		for _, y := range x {
			yy, _ := strconv.Atoi(y)
			count += Cost(yy)
		}
		if min > count {
			min = count
			mini = index
		}
	}

	return mini
}

type solver func(d *Deck, h *Hand, rows []CardSet) int

func NewSolver() solver {
	return solve
}

func randomSolve(d *Deck, h *Hand, rows []CardSet) int {
	return h.Get(rand.Intn(len(h.Cards)))
}

func maxSolve(d *Deck, h *Hand, rows []CardSet) int {
	return h.Get(len(h.Cards) - 1)
}

func defSolve(d *Deck, h *Hand, rows []CardSet) int {
	resulti, resultmin := -1, 9999
	score := make([]int, len(h.Cards))
	for idx, res := range h.Cards {
		min := 9999
		for _, x := range rows {
			_, y := x.Last()
			if y < res && min > res-y && len(x) < 5 {
				min = res - y
			}
		}

		if resultmin > min {
			resulti, resultmin = idx, min
		}
	}
	if resulti >= 0 {
		score[resulti] = 10
	} else {
		resulti, _ := h.Cards.Last()
		score[resulti] = 10
	}

	mi, _ := maxA(score)
	return h.Get(mi)
}

func solve(d *Deck, h *Hand, rows []CardSet) int {
	resulti, resultmin := -1, 9999
	score := make([]int, len(h.Cards))
	for idx, res := range h.Cards {
		min := 9999
		for _, x := range rows {
			_, y := x.Last()
			if y < res && min > res-y && len(x) < 5 {
				min = res - y
			}
		}

		if resultmin > min {
			resulti, resultmin = idx, min
		}
	}
	if resulti >= 0 {
		score[resulti] = 10
	} else if len(h.Cards) < 6 {
		score[simSolveN(d, h, rows)] = 6
	}

	lastCards := getLasts(rows)
	sort.Ints(lastCards)

	max := 0
	for i, v := range lastCards {
		upperi, upper := UpperBound(h.Cards, v)
		prevCard := 0
		if i > 0 {
			prevCard = lastCards[i-1]
		}

		if max < upper-prevCard {
			resulti, max = upperi, upper-prevCard
		}
	}

	if resulti >= 0 {
		score[resulti] = 6
	}

	mi, _ := maxA(score)
	return h.Get(mi)
}

func getLasts(cs []CardSet) []int {
	result := make([]int, len(cs))
	for i, v := range cs {
		_, result[i] = v.Last()
	}

	return result
}

func getLastsWithIndex(cs []CardSet) [][]int {
	result := make([][]int, len(cs))
	for i, v := range cs {
		ii, vv := v.Last()
		result[i] = []int{ii, vv}
	}

	return result
}

func maxA(a []int) (ri int, rv int) {
	for i, v := range a {
		if v >= rv {
			ri, rv = i, v
		}
	}

	return ri, rv
}

func minA(a []int) (ri int, rv int) {
	rv = 99999
	for i, v := range a {
		if v <= rv {
			ri, rv = i, v
		}
	}

	return ri, rv
}

func multiSolve(d *Deck, h *Hand, rows []CardSet) int {
	if len(h.Cards) > 3 {
		return simSolve(d, h, rows)
	}
	return maxSolve(d, h, rows)
}

// -----

type Player struct {
	d int
	h CardSet
}

type Players []Player

type Rows []CardSet


func simSolveN(d *Deck, h *Hand, rows []CardSet) int {
	score := make([]int, len(h.Cards))

	for t := 0; t < 5; t++ {
		hands := d.CreateHands(4, len(h.Cards))

		var P Players
		for i := 0; i < 4; i++ {
			P = append(P, Player{d:0, h:hands[i].Cards})
		}
		my := Player{d:0, h:h.Cards}

		scores := play(my, P, rows)
		for index, damage := range scores {
			score[index] += damage
		}
	}

	mi, _ := minA(score)
	return mi
}

func simSolve(d *Deck, h *Hand, rows []CardSet) int {
	return h.Get(simSolveN(d, h, rows))
}

func play(my Player, P Players, R Rows) []int {
	score := make([]int, len(my.h))

	for vi, v := range my.h {
		for _, v1 := range P[0].h {
			for _, v2 := range P[1].h {
				for _, v3 := range P[2].h {
					for _, v4 := range P[3].h {
						a := []int{v, v1, v2, v3, v4}
						rrr := sel(a, R)
						score[vi] += rrr[0]
					}
				}
			}
		}
	}

	return score
}

func sel(a []int, R Rows) [5]int {
	b := IndexTuples{}
	for i, v := range a {
		b = append(b, IndexTuple{Index:i, Value: v})
	}
	sort.Sort(b)

	score := make([]int, 5)

	for _, v := range b {
		var lasts IndexTuples
		for i, r := range R {
			_, last := r.Last()
			lasts = append(lasts, IndexTuple{ Index: i, Value: last })
		}
		sort.Sort(lasts)

		idx := -1
		for ii, l := range lasts {
			if v.Value < l.Value {
				break
			}
			idx = ii
		}

		cost := 0
		if idx >= 0 {
			if len(R[idx]) > 4{
				cost = countCost(R[idx])
				R[idx] = CardSet{}
			}
			R[idx] = append(R[idx], v.Value)
		} else {
			mini, min := 99999, 99999
			for index, r := range R {
				count := countCost(r)
				if min > count {
					min, mini = count, index
				}
			}

			R[mini] = CardSet{v.Value}
			cost = min
		}

		score[v.Index] += cost
	}

	var ranks IndexTuples
	for i, v := range score {
		ranks = append(ranks, IndexTuple{Index:i, Value:v})
	}

	sort.Sort(ranks)

	var result [5]int
	for i, v := range ranks {
		result[v.Index] = i
	}

	return result
}

func countCost(s CardSet) int {
	result := 0
	for _, c := range s {
	  result += Cost(c)
	}

	return result
}
// end operation.go
