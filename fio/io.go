package fio

import (
	"bufio"
	"fmt"
	"io"
)

// InOut
type InOut struct {
	*bufio.Reader
	*bufio.Writer
}

func NewInOut(r io.Reader, w io.Writer) *InOut {
	return &InOut{bufio.NewReader(r), bufio.NewWriter(w)}
}

// Get Next Line String
func (s *InOut) NextLine() (r string) {
	b, _ := s.ReadByte()
	for b == '\n' {
		b, _ = s.ReadByte()
	}
	buf := make([]byte, 0)
	for ; b != '\n'; b, _ = s.ReadByte() {
		buf = append(buf, b)
	}
	return string(buf)
}

// Print immediately with new line
func (s *InOut) Println(o interface{}) {
	fmt.Println(o)
}
