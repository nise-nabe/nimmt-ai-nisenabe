package util

import (
	"strings"
)

func SplitCards(str string) []string {
	return strings.Split(str, ",")
}

func EmptyResponse() string {
	return ""
}
