package util

import (
	target "."
	"testing"
)

func TestSplitCards(t *testing.T) {
	strs := "10,15,19,21,29,30,52,59,61,70"
	expected := []string{"10", "15", "19", "21", "29", "30", "52", "59", "61", "70"}

	cards := target.SplitCards(strs)

	for i, c := range cards {
		if expected[i] != c {
			t.Errorf("Invalid card \"%s\"", c)
		}
	}
}
