package command

import (
	"../util"
	"os"
	"strconv"
	"strings"
)

func Players(str string) []string {
	strs := strings.Split(str, " ")

	if strs[0] != "players" {
		panic("Invalid Players Command " + strs[0])
		os.Exit(1)
	}

	ns, players := strs[1], strs[2:]
	if n, err := strconv.Atoi(ns); err != nil || n != len(players) {
		panic("Invalid Input \"Players\"")
		os.Exit(1)
	}

	return players
}

func Cards(str string) (cardNum int, cards []int) {
	strs := strings.Split(str, " ")

	if strs[0] != "cards" {
		panic("Invalid Cards Command " + strs[0])
		os.Exit(1)
	}

	cardNum, err := strconv.Atoi(strs[1])

	if err != nil {
		panic(err)
		os.Exit(1)
	}

	for _, v := range util.SplitCards(strs[2]) {
		n, _ := strconv.Atoi(v)
		cards = append(cards, n)
	}

	return
}

func Rows(str string) [][]int {
	strs := strings.Split(str, " ")
	if strs[0] != "rows" {
		panic("Invalid Cards Command " + strs[0])
		os.Exit(1)
	}

	rows := make([][]int, 0)
	for _, x := range strs[1:] {
		row := make([]int, 0)
		for _, v := range util.SplitCards(x) {
			x, _ := strconv.Atoi(v)
			row = append(row, x)
		}

		rows = append(rows, row)
	}

	return rows
}

func Select(str string) [][]string {
	strs := strings.Split(str, " ")

	if strs[0] != "select" {
		panic("Invalid Cards Command " + strs[0])
		os.Exit(1)
	}

	result := make([][]string, len(strs[1:]))
	for index, x := range strs[1:] {
		result[index] = util.SplitCards(x)
	}

	return result
}

func Places(str string) []int {
	strs := strings.Split(str, " ")

	if strs[0] != "places" {
		panic("Invalid Cards Command " + strs[0])
		os.Exit(1)
	}

	result := make([]int, len(strs[1:]))
	for index, x := range strs[1:] {
		result[index], _ = strconv.Atoi(x)
	}

	return result
}
