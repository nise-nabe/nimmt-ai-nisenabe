package command

import (
	target "."
	"testing"
)

func TestPlayers(t *testing.T) {
	players := target.Players("players 5 alpha beta gamma delta epsilon")
	expected := []string{"alpha", "beta", "gamma", "delta", "epsilon"}
	for i, p := range players {
		if expected[i] != p {
			t.Errorf("Invalid player \"%s\"", p)
		}
	}
}

func TestCareds(t *testing.T) {
	cardNum, cards := target.Cards("cards 104 10,15,19,21,29,30,52,59,61,70")
	expectedCards := []string{"10", "15", "19", "21", "29", "30", "52", "59", "61", "70"}

	if cardNum != 104 {
		t.Errorf("Invalid cardNum")
	}

	for i, c := range cards {
		if expectedCards[i] != c {
			t.Errorf("Invalid card \"%s\"", c)
		}
	}

}
